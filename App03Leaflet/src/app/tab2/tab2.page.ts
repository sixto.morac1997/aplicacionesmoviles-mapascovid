import { Component, OnInit, OnDestroy } from '@angular/core';
import * as Leaflet from 'leaflet';
import { antPath } from 'leaflet-ant-path';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})


export class Tab2Page implements OnInit, OnDestroy {
  map: Leaflet.Map;

  constructor() { }

  ngOnInit() { }

  ionViewDidEnter(...args: []) {
    this.map = Leaflet.map('mapId').setView([-2.1864615494448207, -79.92464618942034], 5);
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Angular LeafLet',
    }).addTo(this.map);

    Leaflet.marker([-2.1864, -79.9246]).addTo(this.map).bindPopup('El coloso').openPopup();
    Leaflet.marker([-2.20, -79.89]).addTo(this.map).bindPopup('El chueco').openPopup();

    antPath([[-2.1864, -79.9246], [-2.20, -79.89]],
      { color: '#FF0000', weight: 5, opacity: 0.6 })
      .addTo(this.map);

    this.map.setZoom(14);
  }

  /** Remove map when we have multiple map object */
  ngOnDestroy() {
    this.map.remove();
  }

}