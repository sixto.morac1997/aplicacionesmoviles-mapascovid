import { Component, OnInit, OnDestroy } from '@angular/core';
import * as Leaflet from 'leaflet';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page  {
  
  map: Leaflet.Map;
  propertyList = [];
  constructor() {}

  ngOnInit() { }
  
  ionViewDidEnter(...args: []) {
    this.map = Leaflet.map('mapId').setView([-2.1864615494448207, -79.92464618942034], 5);
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Angular LeafLet',
    }).addTo(this.map);

    fetch('./assets/lugares.json')
      .then(res => res.json())
      .then(lugares => {
        this.propertyList = lugares.properties;
        this.leafletMap();
      })
      .catch(err => console.error(err));
  }

  leafletMap() {
    for (const property of this.propertyList) {
      Leaflet.marker([property.lat, property.long]).addTo(this.map)
        .bindPopup(property.nombre)
        .openPopup();
        
    }
  }

  ngOnDestroy() {
    this.map.remove();
  }
}
